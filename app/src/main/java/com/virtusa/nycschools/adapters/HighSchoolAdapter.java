package com.virtusa.nycschools.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.virtusa.nycschools.R;
import com.virtusa.nycschools.mvvm.model.HighSchool;

import java.util.ArrayList;

// This class is used by Recycler View that holds the list of high schools
// It has its View Holder and the interface to reach the click listener of each element
// of the list.
public class HighSchoolAdapter extends RecyclerView.Adapter<HighSchoolAdapter.ViewHolder> {

    ArrayList<HighSchool> highSchools;

    public HighSchoolAdapter(ArrayList<HighSchool> highSchools) {
        this.highSchools = highSchools;
    }

    public interface OnHighSchoolClickListener{
        void onSelectedHighSchool(int highSchoolID);
    }

    private OnHighSchoolClickListener onHighSchoolClickListener;

    public void setOnHighSchoolClickListener(OnHighSchoolClickListener listener) {
        onHighSchoolClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.high_school_item,parent,false);
        return new ViewHolder(view, onHighSchoolClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        HighSchool highSchool = highSchools.get(position);
        holder.highSchoolNameTextView.setText(highSchool.getSchool_name());
    }

    @Override
    public int getItemCount() {
        return highSchools.size();
    }

    // This class is the View Holder of each element of the complete list
    // It contains and matches the view elements to be prepared for be display on the
    // Recycler View. Also, it has the listener function of each element to
    // request the SAT Grades of each high school.
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView highSchoolNameTextView;

        public ViewHolder(@NonNull View itemView, final OnHighSchoolClickListener listener) {
            super(itemView);
            highSchoolNameTextView = itemView.findViewById(R.id.highSchoolNameTextView);
            highSchoolNameTextView.setOnClickListener(view -> {
                if (listener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        listener.onSelectedHighSchool(position);
                    }
                }
            });
        }
    }
}
