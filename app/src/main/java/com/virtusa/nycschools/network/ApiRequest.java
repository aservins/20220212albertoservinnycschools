package com.virtusa.nycschools.network;

import com.virtusa.nycschools.mvvm.model.HighSchool;
import com.virtusa.nycschools.mvvm.model.SatGrades;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

//This interface was created to reach the data from API
public interface ApiRequest {
    //This first callback is to obtain the complete list of high schools from API
    @GET("resource/s3k6-pzi2.json")
    Call<List<HighSchool>> getHighSchools();

    //This second callback is to obtain the details of the SAT Scores from a given high schools dbn
    //previously choose by the user on the complete list
    @GET("resource/f9bf-2cp4.json")
    Call<List<SatGrades>> getSatGrades(@Query(value = "dbn", encoded = true) String dbn);

}