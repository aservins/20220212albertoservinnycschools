package com.virtusa.nycschools.repository;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.virtusa.nycschools.mvvm.model.HighSchool;
import com.virtusa.nycschools.mvvm.model.SatGrades;
import com.virtusa.nycschools.network.RetrofitRequest;
import com.virtusa.nycschools.network.ApiRequest;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//This class was created to request de data repository from the API using the ApiRequest class
public class DataRepository {

    private final ApiRequest apiRequest;

    public DataRepository() {
        apiRequest = RetrofitRequest.getRetrofitInstance().create(ApiRequest.class);
    }

    //This function is used to fetch the complete list of high schools from API
    //It returns the data obtained from the response
    public LiveData<List<HighSchool>> getHighSchools() {
        final MutableLiveData<List<HighSchool>> data = new MutableLiveData<>();
        apiRequest.getHighSchools()
                .enqueue(new Callback<List<HighSchool>>() {
                    @Override
                    public void onResponse(@NonNull Call<List<HighSchool>> call, @NonNull Response<List<HighSchool>> response) {
                        data.setValue(response.body());
                    }

                    @Override
                    public void onFailure(@NonNull Call<List<HighSchool>> call, @NonNull Throwable t) {
                        data.setValue(null);
                    }
                });
        return data;
    }

    //This function is used to fetch the SAT Grades from a given high school dbn from the API
    //It returns the data obtained from the response
    public LiveData<List<SatGrades>> getSatGrades(String dbn) {
        final MutableLiveData<List<SatGrades>> data = new MutableLiveData<>();
        apiRequest.getSatGrades(dbn)
                .enqueue(new Callback<List<SatGrades>>() {
                    @Override
                    public void onResponse(@NonNull Call<List<SatGrades>> call, @NonNull Response<List<SatGrades>> response) {
                        data.setValue(response.body());
                    }

                    @Override
                    public void onFailure(@NonNull Call<List<SatGrades>> call, @NonNull Throwable t) {
                        data.setValue(null);
                    }
                });
        return data;
    }
}
