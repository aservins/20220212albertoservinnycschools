package com.virtusa.nycschools.mvvm.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.virtusa.nycschools.R;
import com.virtusa.nycschools.adapters.HighSchoolAdapter;
import com.virtusa.nycschools.mvvm.model.HighSchool;
import com.virtusa.nycschools.mvvm.viewmodel.HighSchoolViewModel;

import java.util.ArrayList;

// This is the View for the complete list of high schools.
// It was chosen to show the data in a Recycler View for
// the nature of it and the optimization that it has in big lists.
// It is the View of the MVVM pattern.
public class HighSchoolsActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private final ArrayList<HighSchool> highSchoolsArrayList = new ArrayList<>();
    private HighSchoolViewModel highSchoolViewModel;
    private HighSchoolAdapter highSchoolAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_schools);
        init();
    }

    // This function initiates the elements of this view.
    // In this function is attach the adapter and the layout manager
    // to the recycler view to show the complete list of the high schools.
    private void init() {
        View view = findViewById(android.R.id.content).getRootView();

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        progressBar = findViewById(R.id.progressBar);
        LinearLayoutManager layoutManager = new LinearLayoutManager(HighSchoolsActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        highSchoolAdapter = new HighSchoolAdapter(highSchoolsArrayList);

        highSchoolAdapter.setOnHighSchoolClickListener(highSchoolID -> {
            Intent intent = new Intent(getApplicationContext(), HighSchoolDetailsActivity.class);
            intent.putExtra("HS", highSchoolsArrayList.get(highSchoolID));
            startActivity(intent);
        });

        recyclerView.setAdapter(highSchoolAdapter);
        highSchoolViewModel = new ViewModelProvider(this).get(HighSchoolViewModel.class);
        getHighSchools(view);
    }

    // This function is used to fetch the complete list of high schools.
    // It fetches and loads the list in the recycler view adapter.
    @SuppressLint("NotifyDataSetChanged")
    private void getHighSchools(View view) {
        highSchoolViewModel.getHighSchoolLiveData().observe(this, highSchool -> {
            if (highSchool != null) {
                highSchoolsArrayList.clear();
                highSchoolsArrayList.addAll(highSchool);
                highSchoolAdapter.notifyDataSetChanged();
            }
            else {
                Snackbar.make(view, "ERROR TO LOAD DATA. TRY AGAIN LATER.", Snackbar.LENGTH_LONG).show();
            }
            progressBar.setVisibility(View.GONE);
        });
    }

}