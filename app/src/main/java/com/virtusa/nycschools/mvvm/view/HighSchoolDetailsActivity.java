package com.virtusa.nycschools.mvvm.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.virtusa.nycschools.R;
import com.virtusa.nycschools.mvvm.model.HighSchool;
import com.virtusa.nycschools.mvvm.viewmodel.SatGradesViewModel;

// This is the View for the details of a high school including the SAT Grades
// It is the View of the MVVM pattern.
public class HighSchoolDetailsActivity extends AppCompatActivity {

    private ConstraintLayout sat_grades_layout;
    private TextView mathScoreTextView;
    private TextView readScoreTextView;
    private TextView writeScoreTextView;
    private ProgressBar progressBar;
    private SatGradesViewModel satGradesViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_school_details);
        init();
    }

    // This function initiates the elements of the view.
    // It decided to used XML elements rather than other technics
    // fot the simple of the nature of the project.
    private void init() {
        View view = findViewById(android.R.id.content).getRootView();

        sat_grades_layout = findViewById(R.id.sat_grades_layout);
        sat_grades_layout.setVisibility(View.GONE);
        progressBar = findViewById(R.id.progressBar);

        TextView highSchoolNameTextView = findViewById(R.id.highSchoolNameTextView);
        TextView highSchoolDescriptionTextView = findViewById(R.id.highSchoolDescriptionTextView);
        TextView highSchoolLocationTextView = findViewById(R.id.highSchoolLocationTextView);
        TextView highSchoolPhoneTextView = findViewById(R.id.highSchoolPhoneTextView);
        TextView highSchoolEmailTextView = findViewById(R.id.highSchoolEmailTextView);
        TextView highSchoolWebTextView = findViewById(R.id.highSchoolWebTextView);

        mathScoreTextView = findViewById(R.id.mathScoreTextView);
        readScoreTextView = findViewById(R.id.readScoreTextView);
        writeScoreTextView = findViewById(R.id.writeScoreTextView);

        HighSchool highSchool = getIntent().getExtras().getParcelable("HS");
        highSchoolNameTextView.setText(highSchool.getSchool_name());
        highSchoolDescriptionTextView.setText(highSchool.getOverview_paragraph());
        highSchoolLocationTextView.setText(highSchool.getLocation());
        highSchoolPhoneTextView.setText(highSchool.getPhone_number());
        highSchoolEmailTextView.setText(highSchool.getSchool_email());
        highSchoolWebTextView.setText(highSchool.getWebsite());

        satGradesViewModel = new ViewModelProvider(this).get(SatGradesViewModel.class);
        getSatGrades(view, highSchool.getDbn());
    }

    // This function is used to fetch the SAT Grades given a high school dbn
    // The view is given to used the Snack bar element to show if there is no SAT Grades
    // of the high school. It is a nice element than the Toast, for that reason
    // it was chosen.
    private void getSatGrades(View view, String dbn) {
        satGradesViewModel.getSatGradesLiveData(dbn).observe(this, satGrades -> {
            if (satGrades != null && satGrades.size() != 0) {
                mathScoreTextView.setText(satGrades.get(0).getMath());
                readScoreTextView.setText(satGrades.get(0).getReading());
                writeScoreTextView.setText(satGrades.get(0).getWriting());
                sat_grades_layout.setVisibility(View.VISIBLE);
            }
            else {
                Snackbar.make(view, "SAT GRADES NOT FOUND", Snackbar.LENGTH_LONG).show();
            }
            progressBar.setVisibility(View.GONE);
        });
    }
}