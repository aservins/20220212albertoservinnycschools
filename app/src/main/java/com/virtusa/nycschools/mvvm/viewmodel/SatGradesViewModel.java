package com.virtusa.nycschools.mvvm.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.virtusa.nycschools.mvvm.model.SatGrades;
import com.virtusa.nycschools.repository.DataRepository;

import java.util.List;

// This class is the View Model of the SatGrades.
// In this class is created the data repository object
// to fetch the data form the API.
public class SatGradesViewModel extends AndroidViewModel {

    private DataRepository satGradesRepository;
    private LiveData<List<SatGrades>> satGradesLiveData;

    public SatGradesViewModel(@NonNull Application application) {
        super(application);
    }

    // This function fetch the SAT Grades of a high school dbn and return into a list of LiveData.
    // It return a list because of the JSON responded from the API.
    public LiveData<List<SatGrades>> getSatGradesLiveData (String dbn) {
        satGradesRepository = new DataRepository();
        satGradesLiveData = satGradesRepository.getSatGrades(dbn);
        return satGradesLiveData;
    }
}
