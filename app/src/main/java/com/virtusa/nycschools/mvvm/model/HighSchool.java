package com.virtusa.nycschools.mvvm.model;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//This class was created for the High School data from the API.
//It was implemented as Parcelable for the needs to share objects (data) between the Activities.
public class HighSchool implements Parcelable {

    @Nullable
    @Expose
    @SerializedName("dbn")
    private final String dbn;

    @Nullable
    @Expose
    @SerializedName("school_name")
    private final String school_name;

    @Nullable
    @Expose
    @SerializedName("overview_paragraph")
    private final String overview_paragraph;

    @Nullable
    @Expose
    @SerializedName("location")
    private final String location;

    @Nullable
    @Expose
    @SerializedName("phone_number")
    private final String phone_number;

    @Nullable
    @Expose
    @SerializedName("school_email")
    private final String school_email;

    @Nullable
    @Expose
    @SerializedName("website")
    private final String website;

    public HighSchool(Parcel in) {
        this.dbn = in.readString();
        this.school_name = in.readString();
        this.overview_paragraph = in.readString();
        this.location = in.readString();
        this.phone_number = in.readString();
        this.school_email = in.readString();
        this.website = in.readString();
    }

    @Nullable
    public String getDbn() { return dbn; }

    @Nullable
    public String getSchool_name() {
        return school_name;
    }

    @Nullable
    public String getOverview_paragraph() { return overview_paragraph; }

    @Nullable
    public String getLocation() {
        return location;
    }

    @Nullable
    public String getPhone_number() {
        return phone_number;
    }

    @Nullable
    public String getSchool_email() {
        return school_email;
    }

    @Nullable
    public String getWebsite() {
        return website;
    }

    public static final Parcelable.Creator<HighSchool> CREATOR
            = new Parcelable.Creator<HighSchool>() {

        public HighSchool createFromParcel(Parcel in) {
            return new HighSchool(in);
        }

        public HighSchool[] newArray(int size) {
            return new HighSchool[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(dbn);
        parcel.writeString(school_name);
        parcel.writeString(overview_paragraph);
        parcel.writeString(location);
        parcel.writeString(phone_number);
        parcel.writeString(school_email);
        parcel.writeString(website);
    }
}
