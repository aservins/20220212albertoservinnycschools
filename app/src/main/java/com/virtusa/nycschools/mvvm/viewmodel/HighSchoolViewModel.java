package com.virtusa.nycschools.mvvm.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.virtusa.nycschools.mvvm.model.HighSchool;
import com.virtusa.nycschools.repository.DataRepository;

import java.util.List;

// This class is the View Model of the HighSchools.
// In this class is created the data repository object
// to fetch the data form the API.
public class HighSchoolViewModel extends AndroidViewModel {

    private DataRepository highSchoolsRepository;
    private LiveData<List<HighSchool>> highSchoolLiveData;

    public HighSchoolViewModel(@NonNull Application application) {
        super(application);
        highSchoolsRepository = new DataRepository();
        highSchoolLiveData = highSchoolsRepository.getHighSchools();
    }

    // This function fetch the complete list of high schools and return into a list of LiveData.
    // It return a list because of the JSON responded from the API.
    public LiveData<List<HighSchool>> getHighSchoolLiveData () {
        return highSchoolLiveData;
    }
}
