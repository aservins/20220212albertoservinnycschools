package com.virtusa.nycschools.mvvm.model;

import androidx.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

//This class was created to hold the data from the SAT Scores obtained by the API
//for a given high school dbn
public class SatGrades {

    @Nullable
    @Expose
    @SerializedName("sat_math_avg_score")
    private final String math;

    @Nullable
    @Expose
    @SerializedName("sat_critical_reading_avg_score")
    private final String reading;

    @Nullable
    @Expose
    @SerializedName("sat_writing_avg_score")
    private final String writing;

    public SatGrades(@Nullable String math, @Nullable String reading, @Nullable String writing) {
        this.math = math;
        this.reading = reading;
        this.writing = writing;
    }

    @Nullable
    public String getMath() {
        return math;
    }

    @Nullable
    public String getReading() {
        return reading;
    }

    @Nullable
    public String getWriting() {
        return writing;
    }
}
